# Cours de Python SNT
Quelques cours de Python sous forme de notebooks Jupyter

## Comment faire fonctionner ces cours ?
1. Installer [Anaconda 3](https://www.anaconda.com/products/individual) (ou n'importe quel autre moyen d'utiliser Jupyter)
2. Après l'installation, lancer Jupyter
3. Télécharger les notebooks sur le dépôt Gitlab
4. Importer les notebooks dans l'interface web de Jupyter
5. C'est parti !

## Conditions d'utilisation
Ces cours sont sous licence GPLv3
Ce qui veut dire que vous pouvez utiliser, modifier, transformer ces cours autant que vous le souhaitez. Les seules obligations : mentionner l'auteur original et donner la même licence à ce que vous produisez à partir de ces cours.
N'hésitez pas à contribuer au projet ou à proposer vos modifications ou bugs en Issue !